use std::collections::HashMap;

use widestring::Utf16String;

use super::{string::JsString, ToJsString};

#[derive(Debug, Clone)]
pub struct JsObject {
	pub value: HashMap<String, super::VariableContent>
}

impl ToJsString for JsObject {
	fn to_string(&self) -> JsString {
		JsString{
			value: Utf16String::from_str("[object Object]")
		}
	}
}