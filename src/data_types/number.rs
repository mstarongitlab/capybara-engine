use super::{bool::JsBool, string::JsString, ToJsString};
use widestring::{utf16str, Utf16String};

#[derive(Debug, Clone, Copy)]
pub struct JsNumber {
	pub value: f64
}

pub const NAN: JsNumber = JsNumber{
	value: f64::NAN,
};

// Implements https://tc39.es/ecma262/2023/#table-numeric-type-ops
impl JsNumber {
	pub fn unary_minus(&self) -> JsNumber {
		jsnum_math_op(&self, &self, |x, _| -x)
	}

	pub fn bitwise_not(&self) -> JsNumber {
		jsnum_math_op(&self, &self, |x, _| !i64::from(x.floor() as i64) as f64)
	}

	pub fn exponentiate(&self, x: &JsNumber) -> JsNumber {
		jsnum_math_op(&self, &x, |x, y| x.powf(*y))
	}

	pub fn multiply(&self, x: &JsNumber) -> JsNumber {
		jsnum_math_op(&self, &x, |x, y| x * y)
	}

	pub fn divide(&self, x: &JsNumber) -> JsNumber {
		jsnum_math_op(&self, &x, |x, y| x % y)
	}

	pub fn add(&self, x: &JsNumber) -> JsNumber {
		jsnum_math_op(&self, &x, |x, y| x + y)
	}

	pub fn pre_add(&mut self) -> JsNumber {
		return if self.value.is_nan() {
			return NAN.clone()
		} else {
			self.value += 1.0;
			self.clone()
		}
	}

	pub fn post_add(&mut self) -> JsNumber {
		return if self.value.is_nan() {
			return NAN.clone()
		} else {
			let old = self.clone();
			self.value += 1.0;
			old
		}
	}

	pub fn subtract(&self, x: &JsNumber) -> JsNumber {
		jsnum_math_op(&self, &x, |x, y| x - y)
	}

	pub fn pre_subtract(&mut self) -> JsNumber {
		return if self.value.is_nan() {
			return NAN.clone()
		} else {
			self.value -= 1.0;
			self.clone()
		}
	}

	pub fn post_subtract(&mut self) -> JsNumber {
		return if self.value.is_nan() {
			return NAN.clone()
		} else {
			let old = self.clone();
			self.value -= 1.0;
			old
		}
	}

	pub fn left_shift(&self, x: &JsNumber) -> JsNumber {
		jsnum_math_op(&self, &x, |x, y| ((x.floor() as i64) << (y.floor() as i64)) as f64)
	}
	
	pub fn right_shift_signed(&self, x: &JsNumber) -> JsNumber {
		jsnum_math_op(&self, &x, |x, y| ((x.floor() as i64) >> (y.floor() as i64)) as f64)
	}

	pub fn right_shift_unsigned(&self, x: &JsNumber) -> JsNumber {
		jsnum_math_op(&self, &x, |x, y| (((x.floor() as i64) as u64) >> (y.floor() as i64)) as f64)
	}

	pub fn less_than(&self, x: &JsNumber) -> JsBool {
		jsnum_comp_op(&self, &x, |x, y| x < y)
	}

	pub fn equal(&self, x: &JsNumber) -> JsBool {
		jsnum_comp_op(&self, &x, |x, y| x == y)
	}

	pub fn same_value(&self, x: &JsNumber) -> JsBool {
		JsBool { value: std::ptr::eq(self, x) }
	}

	pub fn bitwise_and(&self, x: &JsNumber) -> JsNumber {
		jsnum_math_op(&self, &x, |x, y| ((x.floor() as i64) & (y.floor() as i64)) as f64)
	}

	pub fn bitwise_xor(&self, x: &JsNumber) -> JsNumber {
		jsnum_math_op(&self, &x, |x, y| ((x.floor() as i64) ^ (y.floor() as i64)) as f64)
	}

	pub fn bitwise_or(&self, x: &JsNumber) -> JsNumber {
		jsnum_math_op(&self, &x, |x, y| ((x.floor() as i64) | (y.floor() as i64)) as f64)
	}

}

impl ToJsString for JsNumber {
	fn to_string(&self) -> JsString {
		if self.value.is_infinite() {
			JsString{
				value: utf16str!("Infinity").to_owned()
			}
		} else if self.value.is_nan() {
			JsString{
				value: utf16str!("NaN").to_owned()
			}
		} else {
			JsString{
				value: Utf16String::from_str(&self.value.to_string())
			}
		}
	}
}

fn jsnum_math_op<F>(x: &JsNumber, y: &JsNumber, f: F) -> JsNumber where
	F: Fn(&f64, &f64) -> f64
{
	if x.value.is_nan() || y.value.is_nan() {
		NAN.clone()
	} else {
		let value = f(&x.value, &y.value);
		JsNumber{
			value
		}
	}
}

fn jsnum_comp_op<F>(x: &JsNumber, y: &JsNumber, f: F) -> JsBool where
	F: Fn(&f64, &f64) -> bool
{
	if x.value.is_nan() || y.value.is_nan() {
		super::bool::FALSE
	} else {
		let value = f(&x.value, &y.value);
		JsBool{
			value
		}
	}
}