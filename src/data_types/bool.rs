use widestring::Utf16String;

use super::{string::JsString, ToJsString};

#[derive(Debug, Clone)]
pub struct JsBool {
	pub value: bool
}

pub const TRUE: JsBool = JsBool{
	value: true,
};

pub const FALSE: JsBool = JsBool{
	value: false,
};

impl ToJsString for JsBool {
	fn to_string(&self) -> JsString {
		JsString { 
			value: if self.value {Utf16String::from("true")} else {Utf16String::from("false")}
		}
	}
}