use self::{big_int::JsBigInt, bool::JsBool, number::JsNumber, object::JsObject, string::JsString, symbol::JsSymbol};

// All Js primitive objects are wrappers around a single value
pub mod bool;
pub mod string;
pub mod symbol;
pub mod number;
pub mod big_int;
pub mod object;
pub mod array;

#[derive(Debug, Default, Clone)]
pub enum VariableContent {
	#[default]
	Undefined,
	Null,
	Value(ValueType),
}

#[derive(Debug, Clone)]
pub enum ValueType {
	Bool(JsBool),
	String(JsString),
	Symbol(JsSymbol),
	Number(JsNumber),
	BigInt(JsBigInt),
	Object(JsObject),
}

pub trait ToJsString {
	fn to_string(&self) -> JsString;
}

impl ToJsString for ValueType {
	fn to_string(&self) -> JsString {
		match &self {
    		ValueType::Bool(x) => x.to_string(),
    		ValueType::String(x) => x.clone(),
    		ValueType::Symbol(x) => todo!(),
    		ValueType::Number(x) => x.to_string(),
    		ValueType::BigInt(x) => x.to_string(),
    		ValueType::Object(x) => x.to_string(),
		}
	}
}