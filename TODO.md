# Todos

## Stage 1

- [ ] Js interpreter (for [EMCA Script 2023](https://tc39.es/ecma262/2023))
    - [ ] Js into token stream/tree
    - [ ] Stack handling
    - [ ] Token stream/tree interpretation

## Stage 2

- [ ] Update interpreter to latest stable ECMA script version

## Stage 3

- [ ] Deep type tracing

## Stage 4

- [ ] Typscript support

## Stage 5

- [ ] Precompile as much as possible

## Stage 6

- [ ] Profit?

## Comments
- Have to add wasm somewhere