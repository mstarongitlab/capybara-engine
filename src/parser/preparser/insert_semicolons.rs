use crate::utils::str_safe_split;

// Takes space normalised source code and adds semicolons where needed
// May not work properly if string isn't space normalised beforehand
pub fn insert_semicolons(raw: &str) -> String {
    let _splits = str_safe_split(raw, vec![' ','\t', '\n']);

    "".to_owned()
}