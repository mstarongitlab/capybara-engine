use num_bigint::BigInt;

const LINE_BREAKS: [char; 4] = ['\u{000A}', '\u{000D}', '\u{2028}', '\u{2029}'];

#[derive(Debug)]
enum OldTokens {
    MinusOne,
    Minus,
    MinusEquals,
    Comma,
    Semicolon,
    Colon,
    Not,
    NotEquals,
    StrongNotEquals,
    DoubleQuestion,
    QuestionQuestionEquals,
    QuestionDot,
    Question,
    Dot,
    TrippleDot,
    BracketOpen,
    BracketClose,
    SquareBracketOpen,
    SquareBracketClose,
    CurlyBracketOpen,
    CurlyBracketClose,
    Multiply,
    PowerOf,
    PowerOfEquals,
    MultiplyEquals,
    Divide,
    DivideEquals,
    BitAnd,
    LogicAnd,
    LogicAndEquals,
    BitAndEquals,
    Hashtag,
    Modulo,
    ModuloEquals,
    BitXOr,
    BitXOrEquals,
    Add,
    AddOne,
    AddEquals,
    LessThan,
    LeftShift,
    LeftShitEquals,
    LessOrEqual,
    EqualAssign,
    EqualCompare,
    EqualStrongCompare,
    Arrow,
    MoreThan,
    MoreOrEqual,
    RightShift,
    RightShiftEquals,
    UnsignedRightShift,
    UnsignedRightShiftEquals,
    BitOr,
    BitOrEquals,
    LogicOr,
    LogicOrEquals,
    Bitflip,
    KeyAwait,
    KeyBreak,
    KeyCase,
    KeyCatch,
    KeyClass,
    KeyComment,
    KeyConst,
    KeyContinue,
    KeyDebugger,
    KeyDefault,
    KeyDelete,
    KeyDo,
    KyeElse,
    KeyEnum,
    KeyExport,
}

#[derive(Debug)]
pub enum Actions {
    Assign,
    Codeflow(Codeflow),
    Raw(RawValue),
}

#[derive(Debug)]
pub enum RawValue {
    Bool(bool),
    Float(f64),
    Int(BigInt),
}

#[derive(Debug)]
pub enum Codeflow {
    Compare(Comparison),
    TryCatch,
    Loop,
}

#[derive(Debug)]
pub enum Comparison {
    WeakEquals,
    StrongEquals,
    WeakNotEquals,
    StrongNotEquals,
    LessThan,
    LessOrEqual,
    MoreThan,
    MoreOrEqual,
}

#[derive(Debug)]
pub enum Keywords {
    Await,
    Break,
    Case,
    Catch,
    Class,
    Const,
    Continue,
    Debugger,
    Default,
    Delete,
    Do,
    Else,
    Enum,
    Export,
    Extends,
    Finally,
}
