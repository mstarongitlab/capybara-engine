#[allow(unused)] // reason: Will be used later
pub const RESERVED_WORDS: [&str; 35] = [
    "break",
    "case",
    "catch",
    "class",
    "const",
    "continue",
    "debugger",
    "default",
    "delete",
    "do",
    "else",
    "export",
    "extends",
    "false",
    "finally",
    "for",
    "function",
    "if",
    "import",
    "in",
    "instanceof",
    "new",
    "null",
    "return",
    "super",
    "switch",
    "this",
    "throw",
    "true",
    "try",
    "typeof",
    "var",
    "void",
    "while",
    "with",
];

#[allow(unused)] // reason: Will be used later
pub const RESERVED_WORDS_EXTENDED_STRICT: [&str; 3] = [
    "let",
    "static",
    "yield",
];

#[allow(unused)] // reason: Will be used later
pub const RESERVED_WORDS_EXTENDED_MODULE_OR_ASYNC: [&str; 1] = [
    "await",
];

#[allow(unused)] // reason: Will be used later
pub const FUTURE_RESERVED_WORDS: [&str; 1] = [
    "enum",
];

#[allow(unused)] // reason: Will be used later
pub const FUTURE_RESERVED_WORDS_EXTENDED_STRICT: [&str; 6] = [
    "implements",
    "interface",
    "package",
    "private",
    "protected",
    "public",
];

#[allow(unused)] // reason: Will be used later
pub const FUTURE_RESERVED_WORDS_OLD_VERSIONS: [&str; 16] = [
    "abstract",
    "boolean",
    "byte",
    "char",
    "double",
    "final",
    "float",
    "goto",
    "int",
    "long",
    "native",
    "short",
    "synchronized",
    "throws",
    "transient",
    "volatile",
];

#[allow(unused)] // reason: Will be used later
pub const SPECIAL_IDENTIFIERS_IN_CONTEXT: [&str; 8] = [
    "arguments",
    "as",
    "async",
    "eval",
    "from",
    "get",
    "of",
    "set",
];

#[allow(unused)] // reason: Will be used later
pub const STRING_QUANTIFIERS: [char; 3] = [
    '"',
    '\'',
    '`',
];