mod clear_spaces;
mod insert_semicolons;

// Prepares the given raw code to be consumed by the tokeniser
// It achieves this by adding missing semicolons and adjusting whitespace (normalising to one space for everything outside of strings)
pub fn preparse(raw: &str) -> String {
    let result = clear_spaces::clear_spaces(raw);
    let result = insert_semicolons::insert_semicolons(&result);

    result
}
