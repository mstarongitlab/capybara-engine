use std::str::Chars;

#[derive(PartialEq)]
enum QuoteMethod {
    Single,
    Double,
    Backtick,
}

// Normalised empty space in codeblocks, ensuring that all whitespace in codeblocks is one character wide
#[allow(unused)] // reason: Will be used later
pub fn clear_spaces(raw: &str) -> String {
    clear_in_code(&mut raw.chars())
}

fn clear_in_code(raw: &mut Chars) -> String {
    let mut cleared = String::new();
    let mut last_char = 'a';
    while let Some(c) = raw.next() {
        match &c {
            '"' => {
                cleared.push('"');
                let res = clear_in_quotes(raw, QuoteMethod::Double);
                cleared.push_str(&res);
                last_char = '"';
            }
            '\'' => {
                cleared.push('\'');
                let res = clear_in_quotes(raw, QuoteMethod::Single);
                cleared.push_str(&res);
                last_char = '\'';
            }
            '`' => {
                cleared.push('`');
                let res = clear_in_quotes(raw, QuoteMethod::Backtick);
                cleared.push_str(&res);
                last_char = '`';
            }
            '{' => {
                cleared.push('{');
                let res = clear_in_code(raw);
                cleared.push_str(&res);
                last_char = '}';
            }
            '}' => {
                cleared.push('}');
                return cleared;
            }
            x => {
                if last_char.is_whitespace() && x.is_whitespace() {
                    last_char = *x;
                    continue;
                }
                cleared.push(*x);
                last_char = *x;
            }
        }
    }

    cleared
}

fn clear_in_quotes(raw: &mut Chars, method: QuoteMethod) -> String {
    let mut cleared = String::new();
    let mut last_char = 'a';
    while let Some(c) = raw.next() {
        match c {
            '"' => {
                cleared.push('"');
                if method == QuoteMethod::Double && last_char != '\\' {
                    return cleared;
                }
                last_char = '"';
            }
            '\'' => {
                cleared.push('\'');
                if method == QuoteMethod::Single && last_char != '\\' {
                    return cleared;
                }
                last_char = '\'';
            }
            '`' => {
                cleared.push('`');
                if method == QuoteMethod::Backtick && last_char != '\\' {
                    return cleared;
                }
                last_char = '`';
            }
            '{' => {
                cleared.push('{');
                // Code blocks are only run inside backtick strings and with a $ before the curly braces
                if method == QuoteMethod::Backtick && last_char == '$' {
                    let res = clear_in_code(raw);
                    cleared.push_str(&res);
                    last_char = '}';
                } else {
                    last_char = '{';
                }
            }
            x => {
                cleared.push(x);
                last_char = x;
            }
        }
    }
    cleared
}

// Test to make sure that multiple spaces get grouped into one
#[test]
fn test_clear_spaces1() {
    assert_eq!(clear_spaces("  \n  \t"), " ".to_owned())
}

// Test to make sure that only spaces are affected and not normal text
#[test]
fn test_clear_spaces2() {
    let res = clear_spaces("foo  bar");
    assert_eq!(res, "foo bar".to_owned())
}

// Test to make sure that double quoted test keeps spaces
#[test]
fn test_clear_spaces3() {
    let res = clear_spaces("\"foo  bar\"");
    assert_eq!(res, "\"foo  bar\"".to_owned())
}

// Same as previous, but for single quotes
#[test]
fn test_clear_spaces4() {
    let res = clear_spaces("'foo  bar'");
    assert_eq!(res, "'foo  bar'".to_owned())
}

// Test to make sure that code blocks inside single quotes are treated as quoted as well
#[test]
fn test_clear_spaces5() {
    let res = clear_spaces("pre quote'foo {shouldn\\'t   compress space} inside'post quote");
    assert_eq!(
        res,
        "pre quote'foo {shouldn\\'t   compress space} inside'post quote".to_owned()
    )
}

// Test to make sure that code blocks inside double quotes are treated as non-quoted
#[test]
fn test_clear_spaces6() {
    let res = clear_spaces("pre quote`foo ${should   compress space} inside`post quote");
    assert_eq!(
        res,
        "pre quote`foo ${should compress space} inside`post quote".to_owned()
    )
}
