# Capybara engine

## What

Capybara engine is a runtime for the JavaScript (ECMAScript) programming language.

The initial target is too conform to ECMAScript 2023 as described on [https://tc39.es/ecma262/2023].

It's also part of a project to maybe make a completely new browser (engine) one day.

## What is not included
- Any sort of APIs not defined in the language specification (like stuff nodejs has or the different browser APIs)
- HTML & DOM systems (HTML rendering will be a seperate module)

## Later features once js runtime is stable
- wasm

## How
- Base featureset included in runtime
- Register functions and objects before execution time
