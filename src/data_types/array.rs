use widestring::Utf16String;

use super::{string::JsString, ToJsString, ValueType};

#[derive(Debug, Clone)]
pub struct JsArray {
	pub values: Vec<ValueType>
}

impl ToJsString for JsArray {
	fn to_string(&self) -> JsString {
		let mut s = Utf16String::from("[");
		for i in self.values.iter() {
			let i = i as &dyn ToJsString;
			s.push_utfstr(&i.to_string().value);
		}
		s += "]";
		JsString {
			value: s
		}
	}
}