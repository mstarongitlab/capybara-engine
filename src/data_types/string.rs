use widestring::Utf16String;

use super::ToJsString;

#[derive(Debug, Clone)]
pub struct JsString {
	pub value: Utf16String,
}

impl ToJsString for JsString {
	fn to_string(&self) -> JsString {
		self.clone()
	}
}